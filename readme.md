# koro

A declarative control flow library for use with JavaScript

## Example

```js
import {koro, call} from 'koro'

let Api = {
  authorize: (user, pass)=> Promise.resolve({token:'00abcdef'}),
  fetchGameData: token=> Promise.resolve({candies:100, lollipops:99})
}

let loadGame = function* (user, pass) {
  let {token} = yield call(Api.authorize, user, pass)
  let {candies, lollipops} = yield call(Api.fetchGameData, token)
  return `You currently have ${candies} candies and ${lollipops} lollipops`
}

koro(loadGame('ashk', 'password1')).then(
  message=> console.log(message),
  err=> console.error(err.message)
)
```

## API Reference

* [Public procedures](#public-procedures)
  * [`koro(iterator)`](#koroiterator)
* [Effects](#effects)
  * [`apply(f, args)`](#applyf-args)
  * [`call(f, ...args)`](#callf-args)
  * [`cps(f, ...args)`](#cpsf-args)
  * [`parallel(effects)`](#paralleleffects)
  * [`race(effects)`](#raceeffects)

### Public Procedures

#### `koro(iterator)`

Automatically steps through iterator by resolving any yielded [Effects](#effects)

- `iterator: {next, throw}` - an Iterator object, Typically created by invoking
  a Generator function
- returns a `Promise`

### Effects

Effects are `yield`ed within a koro iterator

#### `apply(f, args)`

Creates an effect that applies a function

- `f: Function` - A function to be applied. Must return a Promise.
- `args: Array<any>` - Arguments for the function
- resolves one `any`

#### `call(f, ...args)`

Alias for `apply(f, args)`

#### `cps(f, ...args)`

Creates an effect that applies a Node-style function. This is for using koro
with functions that do not return Promises.

- `f: Function` - A function which accepts a callback as its final argument. The
  callback has 2 parameters: `(err, result)`
- `args: Array<any>` - Arguments for the function
- resolves one `any`

Alias for `call(promisify(f), ...args)` where `promisify` is fulfilled by your
lib of choice.

#### `parallel(effects)`

Creates an effect that resolves an array of effects in parallel. Like
`Promise.all`.

- `effects: Array<Effect>` - An array of effects
- resolves an `Array<any>` of each Promise's resolved value

#### `race(effects)`

Creates an effect that resolves (or rejects) the first-to-respond effect. Like
`Promise.race`.

- `effects: Array<Effect>` - An array of effects
- resolves one `any`

## License

BSD-3-Clause
