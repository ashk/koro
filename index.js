// utilities
const sym = Symbol
const pmap = (f,p) => new Promise((pass,fail)=> p.then(x=> pass(f(x)), fail))
const omap = (f,o)=> Object.keys(o).map(k=> f([k, o[k]]))

const promisify = f=> (...args)=> {
  return new Promise((pass,fail) => {
    f.call(null, ...args, (err,x)=> {
      if (err) return fail(err)
      else return pass(x)
    })
  })
}

// symbols
const APPLY = sym('APPLY')
const CPS = sym('CPS')
const PARALLEL = sym('PARALLEL')
const RACE = sym('RACE')

// effects
export const apply = (task, args)=> ({ type: APPLY, task, args })
export const call = (task, ...args)=> ({ type: APPLY, task, args })
export const cps = (task, ...args)=> ({ type: CPS, task, args })
export const parallel = (args)=> ({ type: PARALLEL, task: ()=> {}, args })
export const race = (args)=> ({ type: RACE, task: ()=> {}, args })

// koro
export const koro = g=> {
  return new Promise((pass,fail)=> {
    const affect = ({type, task, args})=> {
      switch (type) {
        case APPLY:
          return task.apply(null, args)
        case CPS:
          return promisify(task)(...args)
        case PARALLEL:
          return Promise.all(args.map(x=> affect(x)))
        case RACE:
          return Promise.race(omap(([k,v])=> pmap(x=> ({[k]: x}), affect(v)), args))
      }
    }
    const next = x=> {
      let {done, value} = g.next(x)
      try { return done ? pass(value) : affect(value).then(next, fail) }
      catch (err) { return fail(err) }
    }
    return next()
  })
}
