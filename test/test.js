import test from 'tape'
import {koro, apply, call, cps, parallel, race} from '../'

test('apply', t=> {
  t.plan(3)
  let f = (a,b,c)=> Promise.resolve(a+b+c)
  let g = function* () {
    let x = yield apply(f, [1, 2, 3])
    let y = yield apply(f, [4, 5, 6])
    t.equal(x, 6)
    t.equal(y, 15)
    return x + y
  }
  koro(g()).then(
    x=> t.equal(x, 21),
    x=> t.fail(x)
  )
})

test('apply error', t=> {
  t.plan(1)
  let f = ()=> { throw Error('bad') }
  let g = function* () { yield apply(f) }
  koro(g()).then(
    x=> t.fail(x),
    x=> t.equal(x.message, 'bad')
  )
})

test('call', t=> {
  t.plan(3)
  let f = (a,b,c)=> Promise.resolve(a+b+c)
  let g = function* () {
    let x = yield call(f, 1, 2, 3)
    let y = yield call(f, 4, 5, 6)
    t.equal(x, 6)
    t.equal(y, 15)
    return x + y
  }
  koro(g()).then(
    x=> t.equal(x, 21),
    x=> t.fail(x)
  )
})

test('call error', t=> {
  t.plan(1)
  let f = ()=> { throw Error('bad') }
  let g = function* () { let x = yield call(f) }
  koro(g()).then(
    x=> t.fail(x),
    x=> t.equal(x.message, 'bad')
  )
})

test('cps', t=> {
  t.plan(3)
  let f = (a,b,c,k)=> k(null, a+b+c)
  let g = function* () {
    let x = yield cps(f, 1, 2, 3)
    let y = yield cps(f, 4, 5, 6)
    t.equal(x, 6)
    t.equal(y, 15)
    return x + y
  }
  koro(g()).then(
    x=> t.equal(x, 21),
    x=> t.fail(x)
  )
})

test('cps error', t=> {
  t.plan(1)
  let f = k=> k(Error('bad'))
  let g = function* () { yield cps(f) }
  koro(g()).then(
    x=> t.fail(x),
    x=> t.equal(x.message, 'bad')
  )
})

test('parallel', t=> {
  t.plan(4)
  let f1 = x=> Promise.resolve(x)
  let f2 = x=> new Promise(pass=> setTimeout(pass, 10, x))
  let f3 = (x,k)=> k(null, x)
  let g = function* () {
    let [x,y,z] = yield parallel([ call(f1, 1), call(f2, 2), cps(f3, 3) ])
    t.equal(x, 1)
    t.equal(y, 2)
    t.equal(z, 3)
    return x + y + z
  }
  koro(g()).then(
    x=> t.equal(x, 6),
    x=> t.fail(x)
  )
})

test('parallel error', t=> {
  t.plan(1)
  let f1 = x=> new Promise(pass=> setTimeout(pass, 10, x))
  let f2 = ()=> { throw Error('bad') }
  let g = function* () {
    let [x,y] = yield parallel([ call(f1, 1), call(f2) ])
    t.fail('should fail before this point')
  }
  koro(g()).then(
    x=> t.fail(x),
    x=> t.equal(x.message, 'bad')
  )
})

test('race', t=> {
  t.plan(1)
  let f = (x,y)=> new Promise(pass=> setTimeout(pass, y, x))
  let g = function* () {
    return yield race({
      'a': call(f, 1, 150),
      'b': call(f, 2, 125),
      'c': call(f, 3, 10)
    })
  }
  koro(g()).then(
    x=> t.deepEqual(x, {c: 3}),
    x=> t.fail(x)
  )
})

test('race error', t=> {
  t.plan(1)
  let f = (x,y)=> new Promise((pass, fail)=> setTimeout(fail, y, x))
  let g = function* () {
    return yield race({
      'a': call(f, Error('a'), 150),
      'b': call(f, Error('b'), 125),
      'c': call(f, Error('c'), 10)
    })
  }
  koro(g()).then(
    x=> t.fail(x),
    x=> t.equal(x.message, 'c')
  )
})

test('nested generator', t=> {
  t.plan(5)
  let f = x=> Promise.resolve(x)
  let g1 = function* () {
    let x = yield call(f, 1)
    let y = yield* g2()
    t.equal(x, 1)
    t.equal(y, 5)
    return x + y
  }
  let g2 = function* () {
    let u = yield call(f, 2)
    let v = yield call(f, 3)
    t.equal(u, 2)
    t.equal(v, 3)
    return u + v
  }
  koro(g1()).then(
    x=> t.equal(x, 6),
    x=> t.fail(x)
  )
})

test('nested generator error', t=> {
  t.plan(1)
  let f = x=> Promise.resolve(x)
  let g1 = function* () { yield* g2() }
  let g2 = function* () { throw Error('bad') }
  koro(g1()).then(
    x=> t.fail(x),
    x=> t.equal(x.message, 'bad')
  )
})

test('stress', t=> {
  t.plan(1)
  let f = x=> Promise.resolve(x + 1);
  let g = function* (x) {
    while(true) {
      if (x < 99999)
        x = yield call(f, x)
      else
        return x;
    }
  }
  koro(g(0)).then(
    x=> t.equal(x, 99999),
    x=> t.fail(x)
  )
})
